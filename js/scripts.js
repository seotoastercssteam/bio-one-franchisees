var onReCaptchaSuccess = function () {
    if (window.innerWidth < 768 && (/iPhone|iPod/.test(navigator.userAgent) && !window.MSStream)) {
        var destElementOffset = $('.g-recaptcha').offset().top - 60;
        $('html, body').animate({scrollTop: destElementOffset}, 0);
    }
};
// Document ready
$(function () {
    $('.g-recaptcha').attr('data-callback', 'onReCaptchaSuccess');
});